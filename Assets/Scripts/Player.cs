﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

		public float speed = 10f;
		public Vector2 maxVelocity = new Vector2 (3, 5);
		public bool standing;
		public float jetSpeed = 15f;
		public float airSpeedMultiplier = .3f;
		private PlayerController controller;
		private Animator animator;
		public AudioClip leftFootSound;
		public AudioClip rightFootSound;
		public AudioClip thudSound;
	public AudioClip rocketSound;

		void Start ()
		{
				controller = GetComponent<PlayerController> ();
				animator = GetComponent<Animator> ();
		}
			
		// Update is called once per frame
		void Update ()
		{
				var forceX = 0f;
				var forceY = 0f;

				var absVelocityX = Mathf.Abs (rigidbody2D.velocity.x);
				var absVelocityY = Mathf.Abs (rigidbody2D.velocity.y);

				if (absVelocityY < .2f) {
						standing = true;
				} else {
						standing = false;
				}

				if (controller.moving.x != 0) {
						if (absVelocityX < maxVelocity.x) {
								var forceValue = speed * controller.moving.x;
								forceX = standing ? forceValue : forceValue * airSpeedMultiplier;
								transform.localScale = new Vector3 (forceX > 0 ? 1 : -1, 1, 1);
						}
						animator.SetInteger ("AnimState", 1);
				} else {
						animator.SetInteger ("AnimState", 0);
				}

				if (controller.moving.y > 0) {
			PlayRocketSound();
						if (absVelocityY < maxVelocity.y) {
								forceY = jetSpeed * controller.moving.y;
						}

						animator.SetInteger ("AnimState", 2);
				} else if (absVelocityY > 0) {
						animator.SetInteger ("AnimState", 3);
				}

				rigidbody2D.AddForce (new Vector2 (forceX, forceY));

		}

	void OnCollisionEnter2D(Collision2D target) {
		if (!standing) {
			var absVelocityX = Mathf.Abs(rigidbody2D.velocity.x);
			var absVelocityY = Mathf.Abs(rigidbody2D.velocity.y);

			if (absVelocityX <= .1f || absVelocityY <= .1f) {
				if (thudSound) {
					AudioSource.PlayClipAtPoint(thudSound, transform.position);
				}
			}
		}
	}

	void PlayLeftFootSound() {
		if (leftFootSound) {
			AudioSource.PlayClipAtPoint(leftFootSound, transform.position);
		}
	}

	void PlayRightFootSound() {
		if (leftFootSound) {
			AudioSource.PlayClipAtPoint(rightFootSound, transform.position);
		}
	}

	void PlayRocketSound() {
		if (!rocketSound || GameObject.Find("RocketSound")) {
			return;
		}

		GameObject gameObject = new GameObject ("RocketSound");
		AudioSource audioSource = gameObject.AddComponent<AudioSource>();
		audioSource.clip = rocketSound;
		audioSource.volume = 0.7f;
		audioSource.Play();

		Destroy(gameObject, rocketSound.length);
	}

}
